features {
  lemma: *;
  phon: *;

  sentid: *;

  cat: V,N,C,CL,P,"P+D","P+PRO",I,PONCT,ET,A,ADV,PRO,D,PREF;
 
  cltype:imp,int;
  det:n,y;
  diat: caus,impers,passif,passif_impers;
  g: f,m;
  initcat:ADV,C,D,P,PONCT,PRO,"P+D";
  initlemma:*;
  initm: pastp,inf;
  initpos:ADV,CC,CS,DET,P,PONCT,PRO,"P+D";
  intrinsimp:y;
  m: imp, ind, inf, pastp, presp, subj, part;
  n: s,p;
  p: "1","2","3";
  pos:
    "01","04","1","10","12","100","171","176","1000","10000",
    ADJ,ADJWH,ADV,ADVWH,CS,CC,CLS,CLO,CLR,DET,DETWH,ET,NC,NPP,P,I,PONCT,PRO,PROREL,PROWH,"P+D","P+PRO",PREF,V,VS,VINF,VPP,VPR,VIMP;
  root:y;
  t: cond, cond_past, fut, fut_ant, impft, past, past_ant, past_comp, plus_pft, pres, pst;
  s: c,p,s,int,ind,rel,poss,neg,part,suj,obj,refl;

}

labels {
  % Relation de précédence phonologique
  SUC@green@bottom, SUCI, INIT_SUC@grey@bottom,

  % Labels internes
  A_OBJ-OBJ@red@bottom, DE_OBJ-OBJ@red@bottom, A_OBJ-P_OBJ@red@bottom,

  % Dépendances syntaxiques Sequoia
   aff,aff.demsuj,arg,ato,ats,aux.caus,aux.pass,aux.tps,a_obj,coord,dep,dep.coord,det,de_obj,dis,
   mod,mod.app,mod.cleft,mod.inc,mod.rel,mod.voc,obj,obj.cpl,obj.p,p_obj.agt,p_obj.o,ponct,suj,
}



% =============================================================================================
%  Initialization modules
% =============================================================================================
include "init.grs";

% =============================================================================================
%  Modules introducing  cross level dependencies
% =============================================================================================
include "trans_dep.grs";

% =============================================================================================
%  Modules introducing close modifiers of adjectives, adverbs and verbs
% =============================================================================================
include "strong_modif.grs";

% =============================================================================================
%  Modules introducing dependencies constituting the kernel of the verb
% =============================================================================================
include "verb_kernel.grs";

% =============================================================================================
%  Modules introducing dependencies constituting the kernel of the noun
% =============================================================================================
include "noun_kernel.grs";

% =============================================================================================
%  Modules introducing direct arguments of verbs
% =============================================================================================
include "direct_argument.grs";

% =============================================================================================
%  Modules introducing indirect arguments via a preposition 
% =============================================================================================
include "prep_dep.grs";

% =============================================================================================
%  Modules introducing indirect arguments via a complementizer
% =============================================================================================
include "cpl_dep.grs";

% =============================================================================================
%  Modules introducing weak syntactic dependencies
% =============================================================================================
include "weak_modif.grs";

% =============================================================================================
%  Modules updating dependencies
% =============================================================================================
include "dep_update.grs";

% =============================================================================================
%  Module cleaning ad hoc tokens and non syntactic dependencies
% =============================================================================================
include "cleaning.grs";

% =============================================================================================
%  Modules adapting the annotation to the Sequoia format
% =============================================================================================
include "sequoia_adapt.grs";

module confluent dep_coord {
  rule r {
    match { CC [pos=CC]; C2[]; s1: CC -[SUC]-> C2; POST[]; s2: C2 -[SUC]-> POST }
    without { CC -[dep.coord]-> * }
    commands {
      del_edge s1; del_edge s2;
      add_edge CC -[dep.coord]-> C2;
      add_edge CC -[SUC]-> POST;
    }
  }
}

sequences {
  full {
%initialisation
    unfold_pos;final_punct;
    %composition; %for MELT only
    phonological;
    %composition; %for MELT only
    dupl_suc;
    prefix;
    quotes;coord;multi_coord;

% close dependencies
     adj-adv_modif;verb_modif;coord;
     verb_aux;verb_aff;verb_clitic;verb_aux;
     compound_det;noun_dep;
     %noun_noun;
     quotes;coord;multi_coord;

% iterative dependencies
     subj_adj;subject;
     head_verb_obj;head_verb_ato; head_verb_obj_inf;
     quotes;coord; % Justification : frwiki_50.1000_00958 ... il fallait justifier le transfert et obtenir l'aval ...

     prep_dep; coord; head_verb_indobj;
     prep_dep_verb; coord; head_verb_indobj_clause;
     cpl_dep; head_verb_obj_cpl_clause;
     quotes;coord;multi_coord;
      
     head_adj_indobj; head_adj_indobj_cpl_clause; head_adv_indobj;
     quotes;coord;multi_coord;
     
     close_verb_obj;
     close_verb_indobj;  close_verb_indobj_clause;
     close_verb_obj_cpl_clause;close_verb_obj_inf;
     close_adj_indobj;close_adj_indobj_cpl_clause;close_adv_indobj;  
     close_noun_modif;close_pred_modif;close_noun_modif_clause;
     
     head_noun_modif;head_pred_modif;head_noun_modif_clause;
     quotes;coord;multi_coord;
     
     subject;
     head_verb_obj;head_verb_ato;
     
     head_verb_indobj;head_verb_indobj_clause;
     cpl_dep;head_verb_obj_cpl_clause;head_verb_obj_inf;
     head_adj_indobj; head_adj_indobj_cpl_clause; head_adv_indobj;
     quotes;coord;multi_coord;
     
     noun_dep; prep_dep;subject;
     close_verb_obj;
     close_verb_indobj; close_verb_indobj_clause;
     close_verb_obj_cpl_clause;close_verb_obj_inf;
     close_adj_indobj; close_adj_indobj_cpl_clause;
     close_noun_modif;close_pred_modif;close_noun_modif_clause;
     
     head_noun_modif;head_pred_modif;head_noun_modif_clause;
     head_appos; close_appos; head_verb_mod;close_verb_mod;
     subject; head_verb_obj; head_verb_ato;
     head_verb_indobj;head_verb_indobj_clause;
     cpl_dep;head_verb_obj_cpl_clause;head_verb_obj_inf;
     head_noun_modif_clause;head_pred_modif;head_verb_mod;
     coord;
     close_verb_obj_cpl_clause;

%dependency updating
     nonrefl_clit_update;refl_clit_update;
     coord;coord_update; % Justifié par : Europar.550_00182  L'Union européenne sera lieu de droits ou ne sera pas.
     mod_voc;
     init_mod;

     dep_coord;

% sequoia adaptation
     contraction;
     clean;
     syntax_adapt;
     mark_root; fpunct; clean_root;

    }

}

% justification of the order [composition;phonological] :  frwiki_50.1000_00006  ... au lieu de secouer...
% justification of the order [phonological; composition] :  frwiki_50.1000_00138   Quant aux diamants,...
% justification of the order [coord; noun_dep]: Europar.550_00304 ...pour le rude mais très bon travail ...
% justification of the order [verb_clitic;verb_aux]: Europar.550_00114 	A-t-on jamais pensé ...
% justification of the order [head_verb_obj_inf; cpl_dep]: Europar.550_00084 ... je pense qu'il ne faut pas se voiler la face.
% justification of the order [verb_indobj; cpl_dep] : Europar.550_00010 ...à mesure que l'Asie bénéficiera d'un niveau de vie accru, ...
% justification of the order [verb_obj_clause;modrel_dep] : Europar.550_00006 ...ce qui nous a permis d'aboutir à des compromis,
% justification of the order [right_prep, quotes, noun_dep]: frwiki_50.1000_00992 dernier résumé : l'"Affaire des piastres"
% justification of the order [right_prep,quotes, noun_dep, subject]: frwiki_50.1000_00976 Le "scandale des piastres" fut ramené... 
% justification of the order [verb_obj_clause;cpl_dep] : Europar.550_00028 Même si de nombreuses évolutions nous avaient permis d'en douter, 
% justification of the order [right_prep,quotes, right_adj]: frwiki_50.1000_00901 ... certains clichés "mis en scène"
% justification of the order [ head_verb_mod; head_noun_modif_clause]:Europar.550_00226 ...ce qui, selon moi, fait défaut jusqu'à présent.
% justification of the last order [quotes, noun_dep, preposition_dep]: annodis.er_00035 ...d'une " réalisation apte à l'épanouissement des petits "
% justification of the order [sequoia_adapt; clean]: Europar.550_00050 ... au moyen du Digital Millenium Corporate Act ...
