
module confluent unfold_pos {
  rule il_imp{match {X[lemma=ilimp]} commands{X.lemma=il; X.intrinsimp=y}} % rule working only for MELT
  rule V { match {X[pos="V"]} without { X[cat=V] } commands { X.cat=V; X.m=ind } }
  rule VS { match {X[pos="VS"]} without { X[cat=V] } commands { X.cat=V; X.m=subj } }
  rule VINF { match {X[pos="VINF"]} without { X[cat=V] } commands { X.cat=V; X.m=inf } }
  rule VPP { match {X[pos="VPP"]} without { X[cat=V] } commands { X.cat=V; X.m=pastp } }
  rule VPR { match {X[pos="VPR"]} without { X[cat=V] } commands { X.cat=V; X.m=presp} }
  rule VIMP { match {X[pos="VIMP"]} without { X[cat=V] } commands { X.cat=V; X.m=imp } }

  rule NC { match {X[pos="NC"]} without { X[cat=N] } commands { X.cat=N; X.s=c } }
  rule NPP { match {X[pos="NPP"]} without { X[cat=N] } commands { X.cat=N;X.det=y; X.s=p } }

  rule CS { match {X[pos="CS"]} without { X[cat=C] } commands { X.cat=C; X.s=s } }
  rule CC { match {X[pos="CC"]} without { X[cat=C] } commands { X.cat=C; X.s=c } }

  rule CLS { match {X[pos="CLS"]} without { X[cat=CL] } commands { X.cat=CL;X.det=y;  X.s=suj } }
  rule CLO { match {X[pos="CLO"]} without { X[cat=CL] } commands { X.cat=CL;X.det=y;  X.s=obj } }
  rule CLR { match {X[pos="CLR"]} without { X[cat=CL] } commands { X.cat=CL;X.det=y;  X.s=refl } }

  rule ADJ { match {X[pos="ADJ"]} without { X[cat=A] } commands { X.cat=A } }
  rule ADJWH { match {X[pos="ADJWH"]} without { X[cat=A] } commands { X.cat=A; X.s=int } }

  rule ADV { match {X[pos="ADV"]} without { X[cat=ADV] } commands { X.cat=ADV } }
  rule ADVWH { match {X[pos="ADVWH"]} without { X[cat=ADV] } commands { X.cat=ADV; X.s=int } }

  rule PRO { match {X[pos="PRO"]} without { X[cat=PRO] } commands { X.cat=PRO;X.det=y } }
  rule PROREL { match {X[pos="PROREL"]} without { X[cat=PRO] } commands { X.cat=PRO;X.det=y;  X.s=rel } }
  rule PROWH { match {X[pos="PROWH"]} without { X[cat=PRO] } commands { X.cat=PRO;X.det=y;  X.s=int } }

  rule DET { match {X[pos="DET"]} without { X[cat=D] } commands { X.cat=D } }
  rule DETWH { match {X[pos="DETWH"]} without { X[cat=D] } commands { X.cat=D; X.s=int } }

  rule copy { match {X[pos=P|"P+D"|"P+PRO"|I|PONCT|ET|PRO|PREF]} without { X[cat=P|"P+D"|"P+PRO"|I|PONCT|ET|PRO|PREF] } commands { X.cat=X.pos } }
  }

% ============================================================================================= 
% Addition of termination marks for sentences without final punctuation.
module confluent final_punct {
  rule add_dummy_final {
    match { N[cat<>PONCT] }
    without { N -[SUC]-> * }
    commands { add_node P: <-[SUC]- N; P.cat=PONCT; P.lemma="DUMMY_PUNCT" }
  }
}


% ============================================================================================= 
% Use of phonological rules in reverse direction.
module confluent phonological{

% If a preposition is contracted with a determiner, they are dissociated.
 lex_rule contracted_preposition(feature $phon, @prepphon, @preplemma, @detphon, @detlemma, @num; file "lexicons/contraction-decomp.lp") {
     match{
     	   PREP[cat="P+D",phon=$phon]; POST[];
	   suc_rel: PREP -[SUC]-> POST
     }
     commands{add_node DET: <-[SUC]- PREP;
              DET.phon=@detphon; DET.lemma=@detlemma;DET.cat=D; DET.g=m; DET.n=@num;
	      PREP.phon=@prepphon; PREP.lemma=@preplemma; PREP.cat=P;PREP.pos=P;
              del_edge suc_rel; add_edge DET -[SUC]-> POST}
 }
}

% ============================================================================================= 
% Duplication of successor relations as initial successor relations.
module confluent dupl_suc {

  rule suc_dupl {
    match { N[]; M[]; N -[SUC]-> M; }
    without {N -[INIT_SUC]-> M}
    commands { add_edge N -[INIT_SUC]-> M; }
  }

}

% ============================================================================================= 
% Building of compound words (only for MELT).
module confluent composition{

% Compound words with two components.
lex_rule compound2_word(feature $phon1, $pos1, $phon2, $pos2, @cat,@pos; file "lexicons/compound2_words.lp") {
     match{
     	   W1[phon=$phon1,pos=$pos1]; W2[phon=$phon2,pos=$pos2]; POST[];
	   suc_rel1: W1 -[SUC]-> W2;
	   suc_rel2: W2 -[SUC]-> POST
     }
     commands{del_edge suc_rel1; del_edge suc_rel2; 
              add_edge W1 -[SUC]-> POST; add_edge W1 -[SUCI]-> W2;
     	      W1.initlemma=W1.lemma; W1.lemma= W1.lemma +"_"+W2.lemma;
              W1.initcat=W1.cat;W1.cat=@cat;
	      W1.initpos=W1.pos;W1.pos=@pos}
 }

% Compound words with three components.
lex_rule compound3_word(feature $phon1,$pos1,$phon2,$pos2,$phon3,$pos3,@cat,@pos; file "lexicons/compound3_words.lp") {
     match{
     	   W1[phon=$phon1]; W2[phon=$phon2]; W3[phon=$phon3];POST[];
	   suc_rel1: W1 -[SUC]-> W2;
	   suc_rel2: W2 -[SUC]-> W3;
	   suc_rel3: W3 -[SUC]-> POST
     }
     commands{del_edge suc_rel1; del_edge suc_rel2; del_edge suc_rel3;
              add_edge W1 -[SUC]-> POST; add_edge W1 -[SUCI]-> W2;add_edge W2 -[SUCI]-> W3;
     	      W1.initlemma=W1.lemma; W1.lemma= W1.lemma +"_"+W2.lemma +"_"+W3.lemma;
              W1.initcat=W1.cat;W1.cat=@cat;
	      W1.initpos=W1.pos;W1.pos=@pos}
 }

}

% ============================================================================================= 
% Building of compound words (only for MELT).
module confluent prefix{

%Composition of prefixes with their following root.
rule prefix_word{
     match{
     	   PRE[]; W1[cat=PREF]; W2[];
	   suc_rel1: PRE -[SUC]-> W1;
	   suc_rel2: W1 -[SUC]-> W2;
     }
     commands{del_edge suc_rel1; del_edge suc_rel2;
              add_edge PRE -[SUC]-> W2; add_edge W2 -[mod]-> W1}
 }

}
