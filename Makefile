self_doc:
	@echo " make gui     --> run GUI on Gold POS-tagged Sequoia"
	@echo " make demo    --> run GUI on a simlple sentence"
	@echo ""
	@echo " make split   --> produce folder sequoia_suc"
	@echo " make one     --> run Grew GUI on a sentence"
	@echo " make all     --> run Grew corpus mode on all sequoia"

# ---------- demo on a simple sentence ----------
demo.melt:
	echo "La souris a été mangée par le chat." | MElt -L -T > demo.melt

demo: demo.melt
	grew gui -grs grs/surf_synt_main.grs -i demo.melt

one:
	grew gui -grs grs/surf_synt_main.grs -i one.melt

# ---------- run GUI on Gold POS-tagged Sequoia ----------
gui: sequoia.surf.gold_pos
	grew gui -grs grs/surf_synt_main.grs -i sequoia.surf.gold_pos


# ---------- generic rules for evaluation ----------
.PRECIOUS:  %.text %.ids %.tok %.melt %.gold_pos %.gold_parse %.melt_pos %.melt_parse

# parsing with gold pos:
%.gold_pos: %.conll
	python tools/conll_to_pos.py $< $@

%.gold_parse: %.gold_pos
	grew_dev transform -grs grs/surf_synt_main.grs -i $< -o $@

# parsing with gold tok + MElt:
%.ids : %.conll
	grep "# sent_id =" $< | sed 's/# sent_id = //' > $@

%.tok : %.conll
	python tools/conll_to_tok.py $< $@

%.melt: %.tok
	cat $< | MElt -L -r | sed 's/\*//g' > $@

%.melt_pos : %.melt %.ids
	#paste `basename $< .melt`.ids $< | tr '\t' '#' > $@
	paste $(word 2,$^) $(word 1,$^) | tr '\t' '#' > $@

%.melt_parse: %.melt_pos
	grew_dev transform -grs grs/surf_synt_main.grs -i $< -o $@

# -------------------------------------------------------------------------------
# TODO: parsing from raw text
%.text : %.conll
	grep "# text =" $< | sed 's/# text = //' > $@

# production of a file "Elle/PRO/il dort/V/dormir" from raw text with MElt
%.rawmelt : %.text
	cat $< | MElt -T -L -r | sed 's/\*//g' > $@





# eval
eval:
	mkdir -p _build
	# retour à l'annotation "Seq 7" des cpd
	grew_dev transform -grs merge_dep_cpd.grs -i sequoia.surf.conll -o _build/sequoia.surf_7.conll
	# restriction au 50 premières phrases
	head -1448 _build/sequoia.surf_7.conll > _build/sequoia.surf_7__50.conll
	# parsing with gold POS
	@make _build/sequoia.surf_7__50.gold_parse
	# parsing with melt POS
	@make _build/sequoia.surf_7__50.melt_parse










# %.melt_pos : %.melt %.ids
# 	paste `basename $< .melt`.ids $< > $@

# -------------------------------------------------------------------------------
# production of a file "Elle/PRO/il dort/V/dormir" from conll
# cut  -> select columns 2 3 5    "la	le	DET"
# sed  -> escape '/' char (like in "le/lui")
# perl -> replace tab by '/'      "la/le/DET"
# perl -> swap 2nd and 3rd        "la/DET/le"
# tr   -> replace newline by '§' for next operation
# perl -> replace §§ by new line
# tr   -> repalce § by space
%.seq : %.conll
	cut -f 2,3,5 $< | sed 's+/+__+g' | perl -pe 's+\t+/+g' | perl -pe 's@^([^/]+)/([^/]+)/([^/]+)\n@\1/\3/\2\n@' | tr '\n' '§' | perl -pe 's/§§/\n/g' | tr '§' ' ' > $@

%.seq_pos : %.seq %.ids
	paste `basename $< .seq`.ids $< > $@

%.pos : %.seq_pos %.melt_pos
	echo "DONE"

clean :
	rm -f *.ids *.text *.melt

# eval:
# 	# retour à l'annotation "Seq 7" des cpd
# 	grew_dev -det -grs merge_dep_cpd.grs -strat "main" -i sequoia.surf.conll -f sequoia.surf_7.conll
# 	# pos version
# 	python conll_to_pos.py sequoia.surf_7.conll sequoia.surf_7.pos
# 	# parse
# 	grew -det -grs grs/surf_synt_main.grs -seq full -old_grs -i sequoia.surf_7.pos -f sequoia.surf_7.parsed
# 	# ---------------- 50 premières phrases ----------------
# 	head -1448 sequoia.surf_7.pos > sequoia.surf_7__50.pos
# 	head -1448 sequoia.surf_7.conll > sequoia.surf_7__50.conll
# 	# parse
# 	grew -det -grs grs/surf_synt_main.grs -seq full -old_grs -i sequoia.surf_7__50.pos -f sequoia.surf_7__50.parsed

melt_eval:
	p conll_to_tok.py sequoia.surf_7.conll melt_pos.sents
	cut -d \# -f 2 melt_pos.sents > melt_pos.text
	cat melt_pos.text | MElt -L -r | sed 's/\*//g' > melt_pos.melt
	grew -det -grs grs/surf_synt_main.grs -seq full -old_grs -i melt_pos.melt -f melt_pos.melt.parsed_nolex




all:
	grew transform -grs rewriting_rules/main.grs -seq full -i sequoia_suc.conll -o sequoia_parsed.conll

