#-*- coding:utf-8 -*-
    
map_cat={u'A':u'adj',u'ADV':u'adv',u'C':u'cs',u'CL':u'pro', u'D':u'det',\
         u'ET':u'_',u'N':u'n',u'P':u'prep',u'P+D':u'prep_det',\
         u'P+PRO':u'prep_pro',u'PONCT':u'punct',u'PRO':u'pro',u'V':u'v'}
map_tense={u'cond':u'cond',u'fut':u'fut',u'impft':u'impft',\
           u'past':u'past',u'pst':u'pres'}
def feat_convert(feat_input):
    feat_list1 = feat_input.split('|')
    feat_list2 = []
    if u'm=part' in feat_list1:
        feat_list1.remove(u'm=part')
        if u't=past' in feat_list1:
            feat_list1.remove(u't=past')
            feat_list1.append(u'm=pastp')
        if u't=pst' in feat_list1:
            feat_list1.remove(u't=pst')
            feat_list1.append(u'm=presp')
    for f in feat_list1:
        elts =f.split('=')
        if elts[0] == u't':
            new_elt=u't='+ map_tense[elts[1]]
            feat_list2.append(new_elt)
        elif elts[0] in [u'g',u'm',u'n',u'p']:
            feat_list2.append(f)
    return('|'.join(feat_list2))
        

import os,re,codecs
path = '/Users/perrier/recherche/GREW_resources/'
path_input = path +'FTB6_CONLL/'
path_output = path +'parsing/FTB6/'
for i in range(1,11):
    number = ('0000'+`i`)[-5:]
    finput_path= path_input+'ftb_'+number+'.conll'
    finput=codecs.open(finput_path, mode='r', encoding='utf-8')
    foutput_path= path_output+'ftb_'+number+'.conll'
    foutput=codecs.open(foutput_path, mode='w', encoding='utf-8')
    foutput.write(u'1\t@\t@\t_\t_\t_\t_\t_\t_\t_\n')
    tokens = finput.readlines()
    for t in tokens:
        elts_t = t.split('\t')
        elts_t[6]=elts_t[0]
        elts_t[0] = unicode(str(int(elts_t[0].encode('ascii'))+1))
        if elts_t[3] == u'CL':
            elts_t[3]=u'pro'
            elts_t[4]=u'pro_clit'
        else:
            elts_t[3] = map_cat[elts_t[3]]
            elts_t[4]= elts_t[3]
        elts_t[5]= feat_convert(elts_t[5])
        elts_t[7] = u'SUC'
        foutput.write('\t'.join(elts_t))
    finput.close()
    foutput.close()
