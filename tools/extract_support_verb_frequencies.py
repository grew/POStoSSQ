# -*- coding: utf-8 -*-
import os,re,codecs
input_path = '/Users/perrier/recherche/deep-sequoia/sequoia-corpus-v6.0/sequoia-corpus.np_conll'
output_path = '/Users/perrier/recherche/GREW_resources/parsing/experiment/lexicon.lp'

def find_support_verb(i,s,lex):
    e=s[i]
    obj = e["lemma"]
    gov = e["gov"]
    gov_entry = s[gov-1]
    if gov_entry["cat"]== u"V":
        support_verb = gov_entry["lemma"] + u"_" + obj
        det_found = False
        j=gov
        while not det_found and j < i:
            entry = s[j]
            if entry["cat"]== u"D" and entry["gov"] == i+1:
                det_found = True
            j +=1
        if not det_found:
            compl_found = False
            j = i +1
            while not compl_found and j < len(s):
                entry = s[j]
                prep = entry["lemma"]
                if entry["cat"] == u"P" and entry["gov"]== gov and entry["funct"] <> u"mod":
                    compl_found = True
                    k = j+1
                    objprep = s[k]
                    while objprep["gov"] <> j+1 and k <len(s)-1:
                        k += 1
                        objprep = s[k]
                j +=1
            if compl_found:
                noun = objprep["lemma"]               
                if lexicon.has_key((support_verb,prep,noun)):
                    lex[support_verb,prep,noun] +=1
                else:
                    lex[support_verb,prep,noun] =1
    return lex        
   
finput=codecs.open(input_path, mode='r', encoding='utf-8')
foutput=codecs.open(output_path, mode='w', encoding='utf-8')
corpus = finput.readlines()
finput.close()
lcorpus = len(corpus)
n=0
lexicon ={}
while n < lcorpus:
    l= corpus[n]
    #print l
    sent=[]
    while l <> '\n':
        lentry = l.split('\t')
        #print lentry
        entry = { "lemma":lentry[2], "cat":lentry[3], "pos":lentry[4], "gov":int(lentry[6]), "funct":lentry[7]}
        #print entry
        sent.append(entry)
        n +=1
        l = corpus[n]
    lsent=len(sent)
    for i in range(lsent):
        entry = sent[i]
        if entry["pos"]== u'NC'and entry['funct']== u'obj':
                lexicon = find_support_verb(i,sent,lexicon)
    n += 1    
for (verb,prep,noun) in lexicon:
    foutput.write(verb +"#"+ prep +"#"+ noun +"##" + unicode(str(lexicon[verb,prep,noun]),"utf-8") +"\n")
    
foutput.close()
    
