import os,re,codecs,shutil
path = '/Users/guillaum/forge/semagramme/grew_resources/parsing'
inpath = path + '/sequoia_full'
fout= codecs.open(path +'/sequoia_short.conll', mode='w', encoding='utf-8')
files = os.listdir(inpath)
max =20
for f in files:
    if f[-5:]=='conll':
        fpath=inpath +'/'+f
        finput=codecs.open(fpath, mode='r', encoding='utf-8')
        tokens = finput.readlines()
        sent_length = len(tokens)
        finput.close()
        if sent_length <= max:
            for l in tokens:
                fout.write(l)
fout.close()
