#!/usr/bin/python

#-*- coding:utf-8 -*-

import os,re,codecs

infile = "sequoia.melt_pos"
outfile = "sequoia_suc.conll"

finput=codecs.open(infile, mode='r', encoding='utf-8')
foutput=codecs.open(outfile, mode='w', encoding='utf-8')

lines = finput.readlines()

for line in lines:
    sp1 = line.strip().split('\t')
    id = sp1[0]
    words = sp1[1].split(' ')
#    foutput.write ("1\t@\t@\t_\t_\tsentid="+id+"\t_\t_\t_\t_\n")
    cpt = 0
    for w in words:
    	items = w.split('/')
    	cpt = cpt+1
        if (cpt == 1) :
            foutput.write (`cpt`+"\t"+items[0]+"\t"+items[2]+"\t_\t"+items[1]+"\tsentid="+id+"\t"+`cpt-1`+"\tSUC\t_\t_\n")
        else: 
    	    foutput.write (`cpt`+"\t"+items[0]+"\t"+items[2]+"\t_\t"+items[1]+"\t_\t"+`cpt-1`+"\tSUC\t_\t_\n")
    foutput.write("\n")

finput.close()
foutput.close()
