let (infile,outfile) = match Sys.argv with
| [| _; i; o|] -> (i,o)
| _ -> failwith "need 2 args"

let in_ch = open_in infile
let out_ch = open_out outfile

let parse_feat f =
	match Str.split (Str.regexp "=") f with
	| [fn; fv] -> (fn,fv)
	| _ -> failwith ("illegal feat: "^f)

let _ =
	try
	while true do
		match input_line in_ch with
		| "" -> Printf.fprintf out_ch "\n";
		| s when s.[0] = '#' -> Printf.fprintf out_ch "%s\n" s;
		| line -> match Str.split (Str.regexp "\t") line with
			| ["1"; word; lemma; _; pos; fs; _; _; _; _] ->
				let feats = List.map parse_feat (Str.split (Str.regexp "|") fs) in
				let sentid = List.assoc "sentid" feats in
				Printf.fprintf out_ch "1\t%s\t%s\t_\t%s\tsentid=%s\t%d\tSUC\t_\t_\n"
					word lemma pos sentid 0			
			| [index; word; lemma; _; pos; _; _; _; _; _] ->
				let i =  int_of_string index in
				Printf.fprintf out_ch "%s\t%s\t%s\t_\t%s\t_\t%d\tSUC\t_\t_\n"
					index word lemma pos (i-1)
			| _ -> failwith ("illegal conll: "^line)
	done
with
| End_of_file -> close_in in_ch; close_out out_ch