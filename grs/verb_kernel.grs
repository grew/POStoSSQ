% Left clitics of verbs.
package verb_left-clitic{

% If an ATS-OBJ clitic immediately precedes a verb, it is an ATS-OBJ dependent of this verb.
  rule verb_ats-obj_left-clit{
    pattern{
      PRE [];
      CLIT [xpos=CLO,form="le"|"l'"];
      V[upos=V];
      suc_rel1: PRE -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> V}
   without{V -[obj|suj|ATS-OBJ|A_OBJ.ATTR-OBJ]-> * }
   commands{
     del_edge suc_rel1; del_edge suc_rel2;
     add_edge V -[ATS-OBJ]-> CLIT; add_edge PRE -[SUC]-> V}
  }

% If an OBJ clitic immediately precedes a verb, it is an OBJ dependent of this verb.
  rule verb_obj_left-clit{
    pattern{
      PRE [];
      CLIT [xpos=CLO,form="la"|"les"];
      V[upos=V];
      suc_rel1: PRE -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> V}
    without{V -[obj|suj|ATS-OBJ|A_OBJ.ATTR-OBJ]-> * }
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[obj]-> CLIT; add_edge PRE -[SUC]-> V}
  }

% If an A_OBJ.ATTR clitic immediately precedes a verb, it is an A_OBJ.ATTR dependent of this verb.
  rule verb_aobj_left-clit{
    pattern{
      PRE [];
      CLIT [xpos=CLO,form="lui"|"leur"];
      V[upos=V];
      suc_rel1: PRE -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> V}
    without{V -[suj]-> * } % id prec
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[a_obj.attr]-> CLIT; add_edge PRE -[SUC]-> V}
  }

% If an A_OBJ or P_OBJ.O clitic immediately precedes a verb, it is an A_OBJ-P_OBJ.LOC dependent of this verb.
  rule verb_aobj-pobj_left-clit{
    pattern{
      PRE [];
      CLIT [xpos=CLO,form="y"|"Y"];
      V[upos=V];
      suc_rel1: PRE -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> V}
    without{V -[suj]-> * } % id prec
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[A_OBJ-P_OBJ.LOC]-> CLIT; add_edge PRE -[SUC]-> V}
  }

% If an OBJ or A_OBJ clitic immediately precedes a verb, it is an A_OBJ.ATTR-OBJ dependent of this verb.
  rule verb_aobj-obj_left-clit{
    pattern{
      PRE [];
      CLIT [xpos=CLO|CLR,form="me"|"m'"|"nous"|"se"|"Se"|"s'"|"S'"|"te"|"t'"|"vous"|"le/lui"];
      V[upos=V];
      suc_rel1: PRE -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> V}
    without{V -[suj]-> * } % id prec
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[A_OBJ.ATTR-OBJ]-> CLIT; add_edge PRE -[SUC]-> V}
  }

% If an OBJ or DE_OBJ clitic immediately precedes a verb, it is a DE_OBJ-OBJ dependent of this verb.
  rule verb_deobj-obj_left-clit{
    pattern{
      PRE [];
      CLIT [xpos=CLO,lemma="en"|"clg|cll"];
      V[upos=V];
      suc_rel1: PRE -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> V}
    without{V -[suj]-> * } % id prec
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[DE_OBJ-OBJ]-> CLIT; add_edge PRE -[SUC]-> V}
  }

% If a subject clitic immediately precedes a verb, it is a subject of this verb.
  rule verb_suj_left-clit{
    pattern{
      PRE [];
      CLIT[upos=CL,xpos=CLS];
      V[upos=V,m=ind|subj];
      suc_rel1: PRE -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> V}
    without{V -[suj]-> * }
    commands{
       del_edge suc_rel1; del_edge suc_rel2;
       add_edge V -[suj]-> CLIT; add_edge PRE -[SUC]-> V}
  }

% If the particle "ne" immediately precedes a verb, it is a modifier of this verb.
  rule verb_mod_ne {
    pattern {
      PRE [];
      NE [upos=ADV, lemma="ne"];
      V[upos=V];
      suc_rel1: PRE -[SUC]-> NE;
      suc_rel2: NE -[SUC]-> V}
    without{ NE2[upos=ADV,form="ne"];V -[mod]-> NE2; }
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[mod]-> NE; add_edge PRE -[SUC]-> V}
  }

}


% =============================================================================================
% Right clitics of verbs.
package verb_right-clitic{

% If an ATS-OBJ clitic immediately follows a verb, it is an ATS-OBJ dependent of this verb.
  rule verb_ats-obj_right-clit{
    pattern{
      CLIT[xpos=CLO,form="le"];
      V[upos=V,m=imp];
      POST[];
      suc_rel1: V -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> POST}
    without{V -[suj]-> * }
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[ATS-OBJ]-> CLIT; add_edge V -[SUC]-> POST}
  }

% If an OBJ clitic immediately follows a verb, it is an OBJ dependent of this verb.
  rule verb_obj_right-clit{
    pattern{
      CLIT[xpos=CLO,form="la"|"les"];
      V[upos=V,m=imp];
      POST[];
      suc_rel1: V -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> POST}
    without{V -[suj]-> * }
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[obj]-> CLIT; add_edge V -[SUC]-> POST}
  }

% If an A_OBJ.ATTR clitic immediately follows a verb, it is an A_OBJ.ATTR dependent of this verb.
  rule verb_aobj_right-clit{
    pattern{
      CLIT[xpos=CLO,form="lui"|"leur"];
      V[upos=V,m=imp];
      POST[];
      suc_rel1: V -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> POST}
    without{V -[suj]-> * }
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[a_obj.attr]-> CLIT; add_edge V -[SUC]-> POST}
  }

% If an A_OBJ or P_OBJ.O clitic immediately follows a verb, it is an A_OBJ-P_OBJ.LOC dependent of this verb.
  rule verb_aobj-pobjo_right-clit{
    pattern{
      CLIT [xpos=CLO,form="y"|"-y"|"Y"];
      V[upos=V];
      POST[];
      suc_rel1: V -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> POST}
    without{V -[suj]-> * } % id prec
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[A_OBJ-P_OBJ.LOC]-> CLIT; add_edge V -[SUC]-> POST}
  }

% If an A_OBJ or OBJ clitic immediately follows a verb, it is an A_OBJ.ATTR-OBJ dependent of this verb.
  rule verb_aobj-obj_right-clit{
    pattern{
      CLIT[xpos=CLO|CLR,form="me"|"-moi"|"m'"|"nous"|"se"|"s'"|"te"|"t'"|"vous"];
      V[upos=V,m=imp];
      POST[];
      suc_rel1: V -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> POST}
    without{V -[suj]-> * }
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[A_OBJ.ATTR-OBJ]-> CLIT; add_edge V -[SUC]-> POST}
  }

% If a DE_OBJ or OBJ clitic immediately follows a verb, it is a DE_OBJ-OBJ dependent of this verb.
  rule verb_deobj-obj_right-clit{
    pattern{
      CLIT[xpos=CLO|CLR,form="en"|"-en"];
      V[upos=V,m=imp];
      POST[];
      suc_rel1: V -[SUC]-> CLIT;
      suc_rel2: CLIT -[SUC]-> POST}
    without{V -[suj]-> * }
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[DE_OBJ-OBJ]-> CLIT; add_edge V -[SUC]-> POST}
  }

% If a subject clitic immediately follows a verb, it is a subject of this verb.
  rule verb_suj_right-clit{
    pattern{
      CLIT[xpos=CLS];
      V[upos=V,m=ind|subj];
      POST[];
      suc_rel1: V -[SUC]-> CLIT;
      suc_rel2:  CLIT -[SUC]-> POST }
    without{V -[suj]-> * }
    commands{
      add_edge V -[suj]-> CLIT; add_edge V -[SUC]-> POST;
      del_edge suc_rel1; del_edge suc_rel2}
}


%filter clitic_remainder{
% pattern{ CL[upos=CL]}
% without{* -[a_obj|A_OBJ.ATTR-OBJ|A_OBJ-P_OBJ|DE_OBJ-OBJ|obj|suj|aff]-> CL}
%}
}

% =============================================================================================
% adverbs and pronouns adjacent to verbs as left modifiers
package verb_left-modif{

% Precondition : an adverb immediately precedes a past participle.
% Action: the adverb is a MOD dependent of the past participle.
  rule pastp_mod_left-adv{
    pattern {
      PRE[];
      V[upos=V,m=pastp];
      ADV[upos=ADV];
      suc_rel1: PRE -[SUC]-> ADV;
      suc_rel2: ADV -[SUC]-> V}
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[mod]-> ADV; add_edge PRE -[SUC]-> V}
  }

% Precondition : an adverb immediately precedes an infinitive; if the immediate predecessor of th adverb is not a prepostion or a conjunction of subordination, the adverb is "que" or "ne_pas" or it is immediately preceded by the causative auxiliary "faire".
% Action: the adverb is a MOD dependent of the infinitive.
  rule inf_mod_left-adv{
    pattern {
      PRE[];
      V[upos=V,m=inf];
      ADV[upos=ADV];
      suc_rel1: PRE -[SUC]-> ADV;
      suc_rel2: ADV -[SUC]-> V}
    without{PRE[xpos<>CS|P,lemma <>"faire"];ADV[lemma <>"ne_pas"|"que"]}
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[mod]-> ADV; add_edge PRE -[SUC]-> V}
  }

% Precondition: a past participle is immediately preceded  by a pronoun from the lexicon below;
% Action: the pronoun is a MOD dependent of the past participle.
  rule pastp_mod_left-pro {
    pattern {
      PRE[];
      V[upos=V,m=inf|pastp];
      PRO[xpos=PRO,lemma=lex.lemma];
      suc_rel1: PRE -[SUC]-> PRO;
      suc_rel2: PRO -[SUC]-> V}
    without{PRO[lemma="tout",n=s]}
    without{PRE[upos=P]}
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[mod]-> PRO; add_edge PRE -[SUC]-> V}
  }
#BEGIN lex
lemma
%-------------
chacun
lui
lui-même
soi
soi-même
tout
#END

% Precondition: an infinitive or a past participle is immediately preceded  by a pronoun from the lexicon below.
% Action: the pronoun is an OBJ dependent of the verb.
  rule verb_obj_left-pro {
    pattern {
      PRE[];
      V[upos=V,m=inf|pastp];
      OBJ[xpos=PRO,form=lex.form];
      suc_rel1: PRE -[SUC]-> OBJ;
      suc_rel2: OBJ -[SUC]-> V;
     }
     without { V -[obj]-> *}
     commands{
       del_edge suc_rel1; del_edge suc_rel2;
       add_edge V -[obj]-> OBJ; add_edge PRE -[SUC]-> V
     }
  }
#BEGIN lex
form
%-----------
rien
tout
#END
}


% =============================================================================================
% adverbs or pronouns adjacent to verbs as right modifiers
package verb_right-modif{

% Precondition : a verb is immediately followed  by an adverb with the type compl.
% Action the adverb is a MOD dependent of the verb.
  rule verb_mod_right-adv1{
    pattern {
      V[upos=V];
      ADV[upos=ADV,adv_compl=true,lemma <>"ne"];
      POST[];
      suc_rel1: V -[SUC]-> ADV;
      suc_rel2: ADV -[SUC]-> POST}
    without{ADV[xpos=ADVWH]}
    without{ADV[adv_adj=true]}
    without{ADV[adv_quant=true]; POST[upos=P,lemma="de"]}
    without{ADV[adv_compl=true]}
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[mod]-> ADV; add_edge V -[SUC]-> POST}
  }

% Precondition : a verb is immediately followed  by an adverb with the type smod.
% Action the adverb is a MOD dependent of the verb.
  rule verb_mod_right-adv2{
    pattern {
      V[upos=V];
      ADV[upos=ADV,adv_smod=true,lemma <>"ne"];
      POST[];
      suc_rel1: V -[SUC]-> ADV;
      suc_rel2: ADV -[SUC]-> POST}
    without{ADV[xpos=ADVWH]}
    without{ADV[adv_adj=true]}
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[mod]-> ADV; add_edge V -[SUC]-> POST}
  }

% Precondition : a verb is immediately followed  by an adverb with the type vmod.
% Action the adverb is a MOD dependent of the verb.
  rule verb_mod_right-adv3{
    pattern {
      V[upos=V];
      ADV[upos=ADV,adv_vmod=true,lemma <>"ne"];
      POST[];
      suc_rel1: V -[SUC]-> ADV;
      suc_rel2: ADV -[SUC]-> POST}
    without{ADV[xpos=ADVWH]}
    %without{ADV[adv_adj=true]} % If ADV is ATS dependent of V, the updating of the dependency will be performed at the end.
    without{ADV[adv_quant=true]; POST[upos=P,lemma="de"]}
    without{ADV[adv_loc=true]; * -[aux.pass|aux.tps]-> V}
    without{ADV[adv_temp=true]; * -[aux.pass|aux.tps]-> V}
    commands{
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[mod]-> ADV; add_edge V -[SUC]-> POST}
  }

% Precondition: a verb is immediately followed  by  a pronoun from the lexicon below.
% Action: the pronoun is a MOD dependent of the verb.
  rule verb_mod_right-pro {
    pattern {
      V[upos=V];
      PRO[xpos=PRO,lemma=lex.lemma];
      POST[];
      suc_rel1: V -[SUC]-> PRO;
      suc_rel2: PRO -[SUC]-> POST}
    without{PRO[form="tout"]}
    commands{
       del_edge suc_rel1; del_edge suc_rel2;
       add_edge V -[mod]-> PRO; add_edge V -[SUC]-> POST}
  }
#BEGIN lex
lemma
%------------
chacun
lui-même
soi-même
tout
#END

}

% =============================================================================================
% Dependencies between verbs and auxiliairies.
package verb_aux{

%If the verb "avoir" in a finite mood is immediately followed by a past participle, it is an AUX.TPS dependent of this participle and its head features are transferred to the participle.
  rule verb_auxtps_fin-avoir{
    pattern{
      PRE[];
      AUX[upos=V,m=imp|ind|subj,lemma="avoir"];
      V[upos=V,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
    commands{
       V.initm=pastp;V.m=AUX.m;
       del_edge suc_rel1; del_edge suc_rel2;
       add_edge V -[aux.tps]-> AUX; add_edge PRE -[SUC]-> V;
       shift_out AUX ==> V}
  }

%If the verb "avoir" in the infinitive or the present participle is immediately followed by a past participle,  it is an AUX.TPS dependent of this participle and its head features are transferred to the participle.
  rule verb_auxtps_non-fin-avoir{
    pattern{
      PRE[];
      AUX[upos=V,m=inf|presp,lemma="avoir"];
      V[upos=V,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
    commands{
       V.initm=pastp;V.m=AUX.m;
       del_edge suc_rel1; del_edge suc_rel2;
       add_edge V -[aux.tps]-> AUX; add_edge PRE -[SUC]-> V;
       shift_out AUX ==> V}
  }

%If the verb "être" in a finite mood is immediately followed by a past participle of a verb from the lexicon "passivable-verb.lp", it is an AUX.PASS dependent of this participle and its head features are transferred to the participle.
  rule verb_auxpass_fin-etre(lex from "lexicons/word_class/passivable_verb.lp"){
    pattern{
      PRE[];
      AUX[upos=V,m=imp|ind|subj,lemma="être"];
      V[upos=V,lemma=lex.lemma,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
     without{SE[xpos=CLR]; AUX -[A_OBJ.ATTR-OBJ]-> SE}%Ex: il s'est acheté une voiture. %Ex: Il s'en est acheté une.
     commands{
       V.initm=pastp;V.m=AUX.m; V.diat=passif;
       del_edge suc_rel1; del_edge suc_rel2;
       add_edge V -[aux.pass]-> AUX; add_edge PRE -[SUC]-> V;
       shift_out AUX ==> V}
  }

%If the verb "être" in the infinitive or the present participle is immediately followed by a past participle of a verb from the lexicon "passivable-verb.lp", it is an AUX.PASS dependent of this participle and its head features are transferred to the participle.
  rule verb_auxpass_non-fin-etre(lex from "lexicons/word_class/passivable_verb.lp"){
    pattern{
      PRE[];
      AUX[upos=V,m=inf|presp,lemma="être"];
      V[upos=V,lemma=lex.lemma,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
    without{SE[xpos=CLR]; AUX -[A_OBJ.ATTR-OBJ]-> SE}%Ex: il s'est acheté une voiture. %Ex: Il s'en est acheté une.
    commands{
      V.initm=pastp;V.m=AUX.m; V.diat=passif;
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[aux.pass]-> AUX; add_edge PRE -[SUC]-> V;
      shift_out AUX ==> V}
  }

%If the verb "être"  in a finite mood is immediately followed by a past participle of a verb from the lexicon "verb_with_aux_etre.lp", it is an AUX.TPS dependent of this participle and its head features are transferred to the participle.
  rule verb_auxtps_fin-etre(lex from "lexicons/word_class/verb_with_aux_etre.lp"){
    pattern{
      PRE[];
      AUX[upos=V,m=imp|ind|subj,lemma="être"];
      V[upos=V,lemma=lex.lemma,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
    without{SE[xpos=CLR]; AUX -[A_OBJ.ATTR-OBJ]-> SE} %Ex: Il s'est monté une exposition. %Ex: Il s'en est monté une.
    without{AUX[initm=pastp]} % frwiki_50.1000_00067# ... avaient en réalité été sortis...
    commands{
      V.initm=pastp;V.m=AUX.m;
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[aux.tps]-> AUX; add_edge PRE -[SUC]-> V;
      shift_out AUX ==> V}
 }

%If the verb "être" in the infinitive or the present participle is immediately followed by a past participle of a verb from the lexicon "verb_with_aux_etre.lp", it is an AUX.TPS dependent of this participle and its head features are transferred to the participle.
  rule verb_auxtps_non-fin-etre(lex from "lexicons/word_class/verb_with_aux_etre.lp"){
    pattern{
      PRE[];
      AUX[upos=V,m=inf|presp,lemma="être"];
      V[upos=V,lemma=lex.lemma,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V;}
    without{SE[xpos=CLR]; AUX -[A_OBJ.ATTR-OBJ]-> SE} %Ex: Il s'est monté une exposition. %Ex: Il s'en est monté une.
    without{AUX[initm=pastp]} % frwiki_50.1000_00067# ... avaient en réalité été sortis...
    commands{
      V.initm=pastp;V.m=AUX.m;
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[aux.tps]-> AUX; add_edge PRE -[SUC]-> V;
      shift_out AUX ==> V }
  }

%If the verb "être" in a finite mood is immediately followed by a past participle and if it immediately follows the reflexive clitic "se", it is an AUX.TPS dependent of this participle and its head features are transferred to the participle.
  rule se_verb_auxtps_fin-etre{
    pattern{
      PRE[];
      SE[xpos=CLR];
      AUX[upos=V,m=imp|ind|subj,lemma="être"];
      AUX -[A_OBJ.ATTR-OBJ]-> SE;
      V[upos=V,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
    commands{
      V.initm=pastp;V.m=AUX.m;
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[aux.tps]-> AUX; add_edge PRE -[SUC]-> V;
      shift_out AUX ==> V}
  }

%If the verb "être" in the infinitive or the present participle is immediately followed by a past participle and if it immediately follows the reflexive clitic "se", it is an AUX.TPS dependent of this participle and its head features are transferred to the participle.
  rule se_verb_auxtps_non-fin-etre{
    pattern{
      PRE[];
      SE[xpos=CLR];
      AUX[upos=V,m=inf|presp,lemma="être"];
      AUX -[A_OBJ.ATTR-OBJ]-> SE;
      V[upos=V,m=pastp];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
    commands{
      V.initm=pastp;V.m=AUX.m;
      del_edge suc_rel1; del_edge suc_rel2;
      add_edge V -[aux.tps]-> AUX; add_edge PRE -[SUC]-> V;
      shift_out AUX ==> V }
   }

%If the verb "faire" in a mood different of the past participle is immediately followed by an infinitive, it is an AUX.CAUS dependent of this participle and its head features are transferred to the infinitive.
  rule verb_auxcaus_faire{
     pattern{
       PRE[];
       AUX[upos=V,m=imp|ind|inf|presp|subj,lemma="faire"];
       V[upos=V,m=inf];
       suc_rel1: PRE -[SUC]-> AUX;
       suc_rel2: AUX -[SUC]-> V}
     %without{QUE[upos=ADV,lemma="que"]; V -[mod]-> QUE}
     commands{
       V.initm=inf;V.m=AUX.m;V.diat=caus;
       del_edge suc_rel1; del_edge suc_rel2;
       add_edge V -[aux.caus]-> AUX; add_edge PRE -[SUC]-> V;
       shift_out AUX ==> V}
  }

%A modal auxiliary in a mood different of the past participle is immediately followed by an infinitive,it is an AUX.MOD dependent of this participle and its head features are transferred to the infinitive.
  rule verb_auxmod_aux{
    pattern{
      PRE[];
      AUX[upos=V,m=ind|inf|presp|subj,lemma="devoir"|"pouvoir"];
      V[upos=V,m=inf];
      suc_rel1: PRE -[SUC]-> AUX;
      suc_rel2: AUX -[SUC]-> V}
   % without{QUE[upos=ADV,lemma="que"]; V -[mod]-> QUE}
    commands{
      V.initm=inf;V.m=AUX.m;
      del_edge suc_rel1; del_edge suc_rel2;
      shift_out AUX =[suj]=> V;
      add_edge V -[aux.mod]-> AUX; add_edge PRE -[SUC]-> V}
  }


}

% =============================================================================================
% Transfer of some moprhological features from the auxiliary to the main verb.
package verb_aux_morph{

% When the auxiliary is not a past participle, its number and person features are transferred to the main verb.
  rule aux-feat_transfer{
    pattern{ AUX[p,n, m <> pastp]; V[n]; V -[aux.tps|aux.pass]->AUX}
    without{V[initn]}
    commands{V.initn = V.n; V.n=AUX.n; V.p =AUX.p}
  }

}
