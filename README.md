# POStoSSQ

**POStoSSQ** is a Graph Rewriting System for the Grew software which can be used to produce dependency syntax of French sentences.
It's a component of a larger system called **Grew-parse-FR**

For a documentation about usage of this GRS,
please refer the [Grew documentation page about **Grew-parse-FR**](http://grew.fr/parsing/)
